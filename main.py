from datetime import datetime
from datetime import timedelta

import discord
from discord.ext import tasks

import config

bot = discord.Bot()

DSE_START_DATE = datetime(year=2023, month=4, day=21, hour=8, minute=30)
DSE_RESULT_RELEASE_DATE = datetime(
    year=2023, month=7, day=19, hour=9, minute=0)
JUPAS_RESULT_RELEASE_DATE = datetime(
    year=2023, month=8, day=9, hour=9, minute=0)


def format_timedelta(timedelta: timedelta, time_str: str):
    d = {"days": timedelta.days}
    d["hours"], remain = divmod(timedelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(remain, 60)
    return time_str.format(**d)


def time_until_text(date: datetime):
    now = datetime.now() + timedelta(hours=8)  # convert to UTC+8
    delta = date - now
    return delta


@bot.event
async def on_ready():
    print("ready")
    activity = discord.Activity(type=discord.ActivityType.watching,
                                name=format_timedelta(time_until_text(DSE_START_DATE), "{days} days until DSE 2023"))
    await bot.change_presence(activity=activity)
    change_activity.start()


@bot.slash_command(name="events", description="check time until dse starting, dse result release or jupas result release")
async def dse(ctx):
    dse_start = format_timedelta(time_until_text(
        DSE_START_DATE), "`{days} days, {hours} hours, {minutes} minutes, {seconds} seconds` until DSE 2023")
    dse_result_release = format_timedelta(time_until_text(
        DSE_RESULT_RELEASE_DATE), "`{days} days, {hours} hours, {minutes} minutes, {seconds} seconds` until DSE 2023 results release")
    jupas_result_release = format_timedelta(time_until_text(
        JUPAS_RESULT_RELEASE_DATE), "`{days} days, {hours} hours, {minutes} minutes, {seconds} seconds` until JUPAS 2023 results release")
    await ctx.respond(f"{dse_start}\n{dse_result_release}\n{jupas_result_release}")


@tasks.loop(hours=1.0)
async def change_activity():
    activity = discord.Activity(type=discord.ActivityType.watching,
                                name=format_timedelta(time_until_text(DSE_START_DATE), "{days} days until DSE 2023"))
    await bot.change_presence(activity=activity)


bot.run(config.token)
